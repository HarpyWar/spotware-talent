# Installation

1. Go to http://www2.informatik.uni-freiburg.de/~cziegler/BX/ and download book ratings database. Import SQL files to a new database `spotware_talent`.

2. Setup database connection settings in `api/settings.yml`

3. Setup rewrite for Silex application depending on your web server http://silex.sensiolabs.org/doc/2.0/web_servers.html

4. Run `composer update` inside `api` directory to download PHP dependencies

5. (OPTIONAL) Run `npm update` inside `ui` directory to download JS dependencies. Then run `ng build` to compile Angular2 app source into `dist` directory.

6. Associate directory `api` with domain `api.spotware-talent.local`

7. Associate directory `ui/dist` with domain `spotware-talent.local`

8. Visit http://spotware-talent.local (login with `admin`/`admin`)


## Live Example

http://spotware-talent.harpywar.com

http://api.spotware-talent.harpywar.com
