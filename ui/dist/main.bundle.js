webpackJsonp([1,4],{

/***/ 332:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 332;


/***/ }),

/***/ 333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(451);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(459);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 450:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_client_service__ = __webpack_require__(63);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(client) {
        this.client = client;
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__(519),
            styles: [__webpack_require__(513)],
            providers: [__WEBPACK_IMPORTED_MODULE_1__api_client_service__["a" /* ApiClientService */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__api_client_service__["a" /* ApiClientService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__api_client_service__["a" /* ApiClientService */]) === 'function' && _a) || Object])
    ], AppComponent);
    return AppComponent;
    var _a;
}());
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(450);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__users_users_component__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__books_books_component__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__overview_overview_component__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__page_navigator_component__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login_component__ = __webpack_require__(454);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__api_client_service__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_router__ = __webpack_require__(299);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var routes = [
    { path: '', redirectTo: '/overview', pathMatch: 'full' },
    { path: 'overview', component: __WEBPACK_IMPORTED_MODULE_7__overview_overview_component__["a" /* OverviewComponent */] },
    { path: 'books', component: __WEBPACK_IMPORTED_MODULE_6__books_books_component__["a" /* BooksComponent */] },
    { path: 'users', component: __WEBPACK_IMPORTED_MODULE_5__users_users_component__["a" /* UsersComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_9__login_login_component__["a" /* LoginComponent */] }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_5__users_users_component__["a" /* UsersComponent */],
                __WEBPACK_IMPORTED_MODULE_6__books_books_component__["a" /* BooksComponent */],
                __WEBPACK_IMPORTED_MODULE_7__overview_overview_component__["a" /* OverviewComponent */],
                __WEBPACK_IMPORTED_MODULE_8__page_navigator_component__["a" /* PageNavigatorComponent */],
                __WEBPACK_IMPORTED_MODULE_9__login_login_component__["a" /* LoginComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_router__["a" /* RouterModule */].forRoot(routes)
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_10__api_client_service__["a" /* ApiClientService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 452:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Book; });
var Book = (function () {
    function Book() {
        this.editMode = false;
    }
    return Book;
}());
//# sourceMappingURL=book.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__books_book__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_client_service__ = __webpack_require__(63);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BooksComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BooksComponent = (function () {
    function BooksComponent(client) {
        this.client = client;
        this.title = "Manage Books";
        this.offset = 0;
        this.limit = 25;
        this.booksCount = 9999; // overall count
        document.title = this.title;
    }
    BooksComponent.prototype.getBooks = function () {
        var that = this;
        this.client.getBooks(this.offset, this.limit)
            .subscribe(function (response) {
            that.books = response.data;
            that.books.push(new __WEBPACK_IMPORTED_MODULE_1__books_book__["a" /* Book */]()); // add new book to the end to show a creation form
            that.booksCount = response.count;
        });
    };
    BooksComponent.prototype.createBook = function (book) {
        book.editMode = null;
        var that = this;
        this.client.saveBook(book)
            .subscribe(function (response) {
            if (response.data) {
                var _book = Object.assign({}, book); // shallow copy
                _book['ISBN'] = response.data; // update id
                that.books.push(_book);
            }
            book.editMode = false;
        });
    };
    BooksComponent.prototype.editBook = function (book) {
        var that = this;
        this.client.saveBook(book)
            .subscribe(function (response) {
        });
    };
    BooksComponent.prototype.deleteBook = function (book) {
        var that = this;
        this.client.deleteBook(book)
            .subscribe(function (response) {
            that.books = that.books.filter(function (item) { return item !== book; });
        });
    };
    BooksComponent.prototype.ngOnInit = function () {
        this.getBooks();
    };
    BooksComponent.prototype.pageChanged = function (page) {
        this.offset = page * this.limit - this.limit;
        this.getBooks();
        //console.log("page changed" + pageId);
    };
    BooksComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            selector: 'app-books',
            template: __webpack_require__(520),
            styles: [__webpack_require__(514)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__api_client_service__["a" /* ApiClientService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__api_client_service__["a" /* ApiClientService */]) === 'function' && _a) || Object])
    ], BooksComponent);
    return BooksComponent;
    var _a;
}());
//# sourceMappingURL=books.component.js.map

/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_client_service__ = __webpack_require__(63);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginComponent = (function () {
    function LoginComponent(client) {
        this.client = client;
        this.title = "Login";
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            selector: 'app-login',
            template: __webpack_require__(521),
            styles: [__webpack_require__(515)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__api_client_service__["a" /* ApiClientService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__api_client_service__["a" /* ApiClientService */]) === 'function' && _a) || Object])
    ], LoginComponent);
    return LoginComponent;
    var _a;
}());
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ 455:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_client_service__ = __webpack_require__(63);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverviewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OverviewComponent = (function () {
    function OverviewComponent(client) {
        this.client = client;
        this.title = "Books Gallery";
        this.offset = 0;
        this.limit = 25;
        this.booksCount = 9999; // overall count
        document.title = this.title;
    }
    OverviewComponent.prototype.getBooks = function () {
        var that = this;
        this.client.getBooks(this.offset, this.limit)
            .subscribe(function (response) {
            that.books = response.data;
            that.booksCount = response.count;
        });
    };
    OverviewComponent.prototype.ngOnInit = function () {
        this.getBooks();
    };
    OverviewComponent.prototype.pageChanged = function (page) {
        this.offset = page * this.limit - this.limit;
        this.getBooks();
        //console.log("page changed" + pageId);
    };
    OverviewComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            selector: 'app-overview',
            template: __webpack_require__(522),
            styles: [__webpack_require__(516)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__api_client_service__["a" /* ApiClientService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__api_client_service__["a" /* ApiClientService */]) === 'function' && _a) || Object])
    ], OverviewComponent);
    return OverviewComponent;
    var _a;
}());
//# sourceMappingURL=overview.component.js.map

/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageNavigatorComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// how many page numbers to display
var PAGE_NAVIGATOR_PAGE_NUMBERS = 11;
var PageNavigatorComponent = (function () {
    function PageNavigatorComponent() {
        this.onPageChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
        this.pageNumbers = [];
    }
    PageNavigatorComponent.prototype.ngOnInit = function () {
        // determine current page
        this.page = Math.ceil(this.offset / this.limit) + 1;
        this.updatePageNumbers();
    };
    PageNavigatorComponent.prototype.maxPage = function () {
        return Math.ceil(this.count / this.limit);
    };
    PageNavigatorComponent.prototype.selectPage = function (pageId) {
        this.page = pageId;
        if (this.page <= 0) {
            this.page = 1;
        }
        if (this.page > this.maxPage()) {
            this.page = this.maxPage();
        }
        this.updatePageNumbers();
        this.onPageChange.emit(this.page);
        return false;
    };
    PageNavigatorComponent.prototype.nextPage = function () {
        this.selectPage(this.page + 1);
    };
    PageNavigatorComponent.prototype.prevPage = function () {
        this.selectPage(this.page - 1);
    };
    // return current page numbers
    PageNavigatorComponent.prototype.updatePageNumbers = function () {
        this.pageNumbers = [];
        var start = this.page - Math.floor(PAGE_NAVIGATOR_PAGE_NUMBERS / 2);
        var end = this.page + Math.floor(PAGE_NAVIGATOR_PAGE_NUMBERS / 2);
        if (end > this.maxPage()) {
            start -= end - this.maxPage();
            end = this.maxPage();
        }
        if (start <= 0) {
            start = 1;
        }
        // FIXME: wrong result with small count
        if (end - start < PAGE_NAVIGATOR_PAGE_NUMBERS - 1) {
            end = PAGE_NAVIGATOR_PAGE_NUMBERS;
        }
        if (end - start < PAGE_NAVIGATOR_PAGE_NUMBERS - 1) {
            start = 1;
        }
        for (var i = start; i <= end; i++) {
            this.pageNumbers.push(i);
        }
        //console.log(this.page, this.maxPage(), start, end);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Number)
    ], PageNavigatorComponent.prototype, "offset", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Number)
    ], PageNavigatorComponent.prototype, "limit", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Number)
    ], PageNavigatorComponent.prototype, "count", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _a) || Object)
    ], PageNavigatorComponent.prototype, "onPageChange", void 0);
    PageNavigatorComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            selector: 'page-navigator',
            template: "\n\t<div class=\"ui right floated pagination menu\">\n        <a class=\"icon item\" *ngIf=\"pageNumbers[0] != 1\" (click)=\"selectPage(1)\">\n          <b>First</b>\n        </a>\n        <a class=\"icon item\" *ngIf=\"page != 1\" (click)=\"prevPage()\">\n          <i class=\"left chevron icon\"></i>\n        </a>\n\n        <a class=\"item\" *ngFor=\"let n of pageNumbers\" [ngClass]=\"{'active': n == page}\"  (click)=\"selectPage(n)\">{{ n }}</a>\n        \n\t\t<a class=\"icon item\" *ngIf=\"page != maxPage()\" (click)=\"nextPage()\">\n          <i class=\"right chevron icon\"></i>\n        </a>\n        <a class=\"icon item\" *ngIf=\"pageNumbers[pageNumbers.length-1] != maxPage()\" (click)=\"selectPage(maxPage())\">\n          <b>Last</b>\n        </a>\n\t</div>\n\t<div class=\"clear\"></div>\n\t"
        }), 
        __metadata('design:paramtypes', [])
    ], PageNavigatorComponent);
    return PageNavigatorComponent;
    var _a;
}());
//# sourceMappingURL=page-navigator.component.js.map

/***/ }),

/***/ 457:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User() {
        this.editMode = false;
    }
    return User;
}());
//# sourceMappingURL=user.js.map

/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__users_user__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_client_service__ = __webpack_require__(63);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UsersComponent = (function () {
    function UsersComponent(client) {
        this.client = client;
        this.title = "Manage Users";
        this.offset = 0;
        this.limit = 25;
        this.usersCount = 9999; // overall count
        document.title = this.title;
    }
    UsersComponent.prototype.getUsers = function () {
        var that = this;
        this.client.getUsers(this.offset, this.limit)
            .subscribe(function (response) {
            that.users = response.data;
            that.users.push(new __WEBPACK_IMPORTED_MODULE_1__users_user__["a" /* User */]()); // add new user to the end to show a creation form
            that.usersCount = response.count;
        });
    };
    UsersComponent.prototype.createUser = function (user) {
        user.editMode = null;
        var that = this;
        this.client.saveUser(user)
            .subscribe(function (response) {
            if (response.data) {
                var _user = Object.assign({}, user); // shallow copy
                _user['User-ID'] = response.data; // update id
                that.users.push(_user);
            }
            user.editMode = false;
        });
    };
    UsersComponent.prototype.editUser = function (user) {
        var that = this;
        this.client.saveUser(user)
            .subscribe(function (response) {
        });
    };
    UsersComponent.prototype.deleteUser = function (user) {
        var that = this;
        this.client.deleteUser(user)
            .subscribe(function (response) {
            that.users = that.users.filter(function (item) { return item !== user; });
        });
    };
    UsersComponent.prototype.ngOnInit = function () {
        this.getUsers();
    };
    UsersComponent.prototype.pageChanged = function (page) {
        this.offset = page * this.limit - this.limit;
        this.getUsers();
        //console.log("page changed" + pageId);
    };
    UsersComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            selector: 'app-users',
            template: __webpack_require__(523),
            styles: [__webpack_require__(517)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__api_client_service__["a" /* ApiClientService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__api_client_service__["a" /* ApiClientService */]) === 'function' && _a) || Object])
    ], UsersComponent);
    return UsersComponent;
    var _a;
}());
//# sourceMappingURL=users.component.js.map

/***/ }),

/***/ 459:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 513:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(52)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 514:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(52)();
// imports


// module
exports.push([module.i, "td button {\r\n\twidth: 60px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 515:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(52)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 516:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(52)();
// imports


// module
exports.push([module.i, ".book-container {\r\n\tfloat: left; \r\n\tmargin-right: 30px;\r\n\theight: 380px;\r\n\twidth: 200px;\r\n}\r\n\r\n.book-thumb {\r\n\twidth: 100%;\r\n\theight: 200px;\r\n\ttext-align: center;\r\n\tbackground: #FAFAFA;\r\n\tbackground-image: url(" + __webpack_require__(542) + ");\r\n\tbackground-position: center;\r\n\tbackground-repeat: no-repeat;\r\n\tborder: 1px solid #e1e1e1;\r\n\tborder-bottom: 0;\r\n\tpadding-top: 12px;\r\n}\r\n.book-thumb {\r\n\theight: 184px;\r\n}\r\n\r\n.book-info {\r\n\tpadding-top: 3px;\r\n\tfont-size: 12px;\r\n}\r\n.book-info.title {\r\n\tfont-size: 16px;\r\n}\r\n.book-info.author {\r\n\tfont-size: 13px;\r\n}\r\n.book-info hr {\r\n\tmargin: 3px;\r\n\tpadding: 0;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 517:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(52)();
// imports


// module
exports.push([module.i, "td button {\r\n\twidth: 60px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 519:
/***/ (function(module, exports) {

module.exports = "<div class=\"ui secondary pointing menu\">\n\n  <a class=\"item\" routerLink=\"/overview\" routerLinkActive=\"active\">\n    Gallery\n  </a>\n  <a class=\"item\" routerLink=\"/books\" routerLinkActive=\"active\" *ngIf=\"client.loggedIn()\">\n    Books\n  </a>\n  <a class=\"item\" routerLink=\"/users\" routerLinkActive=\"active\" *ngIf=\"client.loggedIn()\">\n    Users\n  </a>\n  <div class=\"right menu\">\n    <a class=\"ui item\" routerLink=\"/login\" routerLinkActive=\"active\" *ngIf=\"!client.loggedIn()\">Login</a>\n    <a class=\"ui item\" *ngIf=\"client.loggedIn()\" (click)=\"client.logout()\">Logout</a>\n  </div>\n</div>\n<div class=\"loading-overlay\" *ngIf=\"loading\">\n    <!-- show something fancy here, here with Angular 2 Material's loading bar or circle -->\n    Loading...\n</div>\n\n<div class=\"contents\">\n  <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ 520:
/***/ (function(module, exports) {

module.exports = "<h1>{{ title }}</h1>\n\n<hr />\n\n<page-navigator [offset]=\"offset\" [limit]=\"limit\" [count]=\"booksCount\" (onPageChange)=\"pageChanged($event)\"></page-navigator>\n\n<hr />\n\n<table class=\"ui celled table\">\n  <thead>\n    <tr>\n      <th>ISBN</th>\n      <th>Title</th>\n      <th>Author</th>\n      <th>Year</th>\n      <th>Publisher</th>\n      <th></th>\n    </tr>\n  </thead>\n  <tbody *ngFor=\"let book of books\">\n    <tr *ngIf=\"book['ISBN'] && !book.editMode\">\n      <td>{{ book['ISBN'] }}</td>\n      <td>{{ book['Book-Title'] }}</td>\n      <td>{{ book['Book-Author'] }}</td>\n      <td>{{ book['Year-Of-Publication'] }}</td>\n      <td>{{ book['Publisher'] }}</td>\n      <td><button class=\"ui button small\" (click)=\"book.editMode = true\">Edit</button></td>\n    </tr>\n    <tr *ngIf=\"!book['ISBN'] || book.editMode\">\n      <td style=\"border: 2px solid silver; border-right: 0;\" colspan=\"5\">\n        <div class=\"ui form\">\n        <div class=\"fields\">\n\n          <div class=\"nine wide field\">\n            <label>Title</label>\n            <input type=\"text\" [(ngModel)]=\"book['Book-Title']\" value=\"{{ book['Book-Title'] }}\">\n          </div>\n          <div class=\"two wide field\">\n            <label>Author</label>\n            <input type=\"text\" [(ngModel)]=\"book['Book-Author']\" value=\"{{ book['Book-Author'] }}\">\n          </div>\n          <div class=\"two wide field\">\n            <label>Year</label>\n            <input type=\"number\" [(ngModel)]=\"book['Year-Of-Publication']\" value=\"{{ book['Year-Of-Publication'] }}\">\n          </div>\n          <div class=\"four wide field\">\n            <label>Publisher</label>\n            <input type=\"text\" [(ngModel)]=\"book['Publisher']\" value=\"{{ book['Publisher'] }}\">\n          </div>\n        </div>\n        <div class=\"fields\">\n          <div class=\"six wide field\">\n            <label>URL Image Small</label>\n            <input type=\"text\" [(ngModel)]=\"book['Image-URL-S']\" value=\"{{ book['Image-URL-S'] }}\">\n          </div>\n          <div class=\"six wide field\">\n            <label>URL Image Medium</label>\n            <input type=\"text\" [(ngModel)]=\"book['Image-URL-M']\" value=\"{{ book['Image-URL-M'] }}\">\n          </div>\n          <div class=\"six wide field\">\n            <label>URL Image Large</label>\n            <input type=\"text\" [(ngModel)]=\"book['Image-URL-L']\" value=\"{{ book['Image-URL-L'] }}\">\n          </div>\n        </div>\n\n        </div>\n      </td>\n      <td style=\"border: 2px solid silver; border-left: 0;\">\n          <div *ngIf=\"book['ISBN']\">\n            <button class=\"ui positive button small\" (click)=\"editBook(book); book.editMode = false\">S a v e</button>\n            <br /><br />\n            <button class=\"ui button small\" (click)=\"book.editMode = false\">Cancel</button>\n            <br /><br />\n            <button class=\"ui red button small\" (click)=\"deleteBook(book); book.editMode = false\">Delete</button>\n          </div>\n          <div *ngIf=\"!book['ISBN']\">\n            <button class=\"ui positive button small\" (click)=\"createBook(book)\" *ngIf=\"!book['ISBN']\" [disabled]=\"book.editMode === null\">Create</button>\n          </div>\n      </td>\n    </tr>\n  </tbody>\n</table>\n\n<div class=\"clear\"></div>\n"

/***/ }),

/***/ 521:
/***/ (function(module, exports) {

module.exports = "<div style=\"max-width: 300px;\">\n<h1>{{ title }}</h1>\n\n<form class=\"ui form\">\n  <div class=\"field\">\n    <label>User Name</label>\n    <input #username type=\"text\" placeholder=\"User Name\">\n  </div>\n  <div class=\"field\">\n    <label>Password</label>\n    <input #password type=\"text\" placeholder=\"Password\">\n  </div>\n  <button class=\"ui button\" (click)=\"client.login(username.value, password.value)\">Login</button>\n</form>\n\n</div>"

/***/ }),

/***/ 522:
/***/ (function(module, exports) {

module.exports = "<h1>{{ title }}</h1>\n\n\n\n\n<hr />\n\nBooks on page\n<select [(ngModel)]=\"limit\" class=\"ui dropdown\" (change)=\"getBooks(offset, $event.target.value)\">\n  <option value=\"25\">25</option>\n  <option value=\"50\">50</option>\n  <option value=\"100\">100</option>\n  <option value=\"500\">500</option>\n</select>\n\n\n<page-navigator [offset]=\"offset\" [limit]=\"limit\" [count]=\"booksCount\" (onPageChange)=\"pageChanged($event)\"></page-navigator>\n\n<hr />\n\n<div *ngFor=\"let book of books\">\n\n<div class=\"book-container\">\n  <div class=\"book-thumb\">\n    <div style=\"position: absolute; margin-top: -13px; margin-left: 160px;  width: 40px; height: 35px; background: #FFF467; font-size: 18px; padding: 5px; cursor: help\" title=\"Rating\">{{ (book['Book-Rating'] ? book['Book-Rating'] : 0) | number:'1.1-1' }}</div>\n    <img [src]=\"book['Image-URL-M']\" border=\"0\" title=\"{{ book['Book-Title'] }}\" alt=\"{{ book['Book-Title'] }}\"  />\n  </div>\n  <div class=\"book-info title\">{{ book['Book-Title'] }}</div>\n  <div class=\"book-info author\">by {{ book['Book-Author'] }}</div>\n  <div class=\"book-info\">\n    <hr />\n    <i>Published in  {{ book['Year-Of-Publication'] }}\n    \n    <span [hidden]=\"!book['Publisher']\"> ({{ book['Publisher'] }})</span></i>\n  </div>\n</div>\n\n</div>\n\n<div class=\"clear\"></div>\n"

/***/ }),

/***/ 523:
/***/ (function(module, exports) {

module.exports = "<h1>{{ title }}</h1>\n\n<hr />\n\n<page-navigator [offset]=\"offset\" [limit]=\"limit\" [count]=\"usersCount\" (onPageChange)=\"pageChanged($event)\"></page-navigator>\n\n<hr />\n\n<table class=\"ui celled table\">\n  <thead>\n    <tr>\n      <th>ID</th>\n      <th>Location</th>\n      <th>Age</th>\n      <th></th>\n    </tr>\n  </thead>\n  <tbody *ngFor=\"let user of users\">\n    <tr *ngIf=\"user['User-ID'] && !user.editMode\">\n      <td>{{ user['User-ID'] }}</td>\n      <td>{{ user['Location'] }}</td>\n      <td>{{ user['Age'] }}</td>\n      <td><button class=\"ui button small\" (click)=\"user.editMode = true\">Edit</button></td>\n    </tr>\n    <tr *ngIf=\"!user['User-ID'] || user.editMode\">\n      <td style=\"border: 2px solid silver; border-right: 0;\" colspan=\"3\">\n        <div class=\"ui form\">\n        <div class=\"fields\">\n          <div class=\"nine wide field\">\n            <label>Location</label>\n            <input type=\"text\" [(ngModel)]=\"user['Location']\" value=\"{{ user['Location'] }}\">\n          </div>\n          <div class=\"two wide field\">\n            <label>Age</label>\n            <input type=\"number\" [(ngModel)]=\"user['Age']\" value=\"{{ user['Age'] }}\">\n          </div>\n        </div>\n\n        </div>\n      </td>\n      <td style=\"border: 2px solid silver; border-left: 0;\">\n          <div *ngIf=\"user['User-ID']\">\n            <button class=\"ui positive button small\" (click)=\"editUser(user); user.editMode = false\">S a v e</button>\n            <br /><br />\n            <button class=\"ui button small\" (click)=\"user.editMode = false\">Cancel</button>\n            <br /><br />\n            <button class=\"ui red button small\" (click)=\"deleteUser(user); user.editMode = false\">Delete</button>\n          </div>\n          <div *ngIf=\"!user['User-ID']\">\n            <button class=\"ui positive button small\" (click)=\"createUser(user)\" *ngIf=\"!user['User-ID']\" [disabled]=\"user.editMode === null\">Create</button>\n          </div>\n      </td>\n    </tr>\n  </tbody>\n</table>\n\n<div class=\"clear\"></div>\n"

/***/ }),

/***/ 542:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgCAYAAACLz2ctAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAGwElEQVR4Xu2dh47jRhAF1zn7nHM4Z8MBzjmec845/f9n+BXsAQhaFIOaslZbBTzsiSIlUnyc6elp8k5ERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERkTS6Oroquj86pybo2ujSSHcB8N0XPRe9HH6rJejs6H10WyUK4gh+Kvox+i35Xk/Vr9HJ0XSQL4ep9LPo++iP6Kfo6wpBqs76JMN+f0RsR3bEsBAM+HmFArur3ooeje9Sgnoi+iP6K3ow04A50DchV/UpEcE1sqDbrtogYUAMW0DcgMc3VkQxzS/RBpAEL0IDz0YCFLDHgRVHrjs4iGrCQqQa8JCLdQN7r+ej1f/8SlLeY8aygAQuZYkByhfdFF6KfI9IP/PiI129Fd0RnxYQasJAxA9LyYb6PIxLVXfMhXrOcE3J7RPd87GjAQsYMSPfKj7zJfE3NhKRwroyOHQ1YyJgBifE+i4bM18QsCrkxTs6xowEL2WZAulPmiZl6GjMg738eEQseOxqwkGoD3hkdOxqwkLEu+N6ILpgudsiELOd9Biq3Rvuk5SP3OfjRgIWMGfCa6LXolwij9U3YzEc65qWIwtZ9gOHYN1pcLpIbI45lH2jAQsYMSBqG5DMDDEzWbwmb+Tghd0X7aIlo8TjpFNGy31TxUEzLfu5jFK4BCxkzIFwecXKpAKZW8McI0/GX15wERsv7SkSTGno2+jZqFwR/CRUejNjfNdGAhUwxILAe8d0jES0P6z0TUTtIST8t5T64ImIfMBstX2uN+cv+M1tDQnzNi0EDFjLVgA2MRjdH/IUZ9tXqAVOCxHuc/KFZGSq6SYhjirXCAQ1YyFwD/l9gJlpaBkSYrG++JpZ/Fz0VrRUPasBCTosBMdOTETnJ/kCoL96nZP7+aI3QQAMWchoMSNeLmT6NxsyHeJ/48N2I9Ew1GrCQQzcgMSbTe9wshan6ZtsmRuovRMSqlWjAQg7ZgMR9nFwGFZhprOXri/VJFXEXW+UTDDRgIYdsQGZViPvI9801X1dsf3dUNWLXgIUcqgFJJj8QfRIR9/VNNUdsTzx4c1SBBixkLQPyuZwoBgFz83GMXJnWI6nMPvUNNVe0nnThVXPVGrCQNQzIzUvEXbQ63C/C9NjUz8SsN0SYhfhtl663K1pB8oMcK8e8CxqwkGoDMk/LyJO4i9kKPpPnqRDLTUkMM2Jlf9imO9W2q/gcTMgUHrMpu+QHNWAhlQYkbqPla4OGJk78VxHzxtu64zbV9lG07R6UpeLzOEZaZVrZpWjAQqoMyAiTuI2i1H7Lxb9ZhrGGbt9kGcUOmIP9qDYf4jPRD9HT0dKpOg1YSIUB2zwtjyobytexjPdejfonjO2JG1t93xrma+KzaZHp4qmqWRIPasBCKgxIZcyLURs0DBmQE89cLvFgd3aCfWCgwj0lrNPftlrsS2uRqaieO0rXgIXsakDiNloSClMxzybzNbUTz0CgFQp0p9rWiPuGxPdwmwHVNTwbew4asJBdDYh5OBlTzcM6fA85PrZtXTfLNq2/ltgPRGqGeHDOVJ0GLGQXAxLEd+O+KQZErEdNHyZke7ruTeutLfajjdD5DaaiAQtZakBiOJLFrTh0qvlQW5/vw7z7iPuGxH60sGDqPc0asJAlBiR2ezTq5vv6J3ZMbbul21epfT/xII+cI5E+hgYsZIkBydfNifsOXRwDrfDUeFADFjLXgOTryOUt6XoPWc2EpIJICW2bqtOAhcwxIJUk3IpJ1zuWcjmN4njoitsIfah+UAMWMtWArEfLQLB+LF1vXxwTYqqOAdZQflADFjLFgMwUEPe9Ew09I+ZYxHHRunNXHQn2TU9Z0ICFTDEgr3kgOS3DMZuvieMjNUNhBY8n7nfFGrCQMQPyfsV9GadRhBqbSvk1YCHbDEjX230+YP8EHbu44JilYeDVLZ7QgIVsMyBBOMnZbtx3FsVUHQOwVjWjAQsZMiB5MJbz49MKnGUR+zIAw3igAQvZZEDq+8j5UTJF/Kf+udWg/T8oGrCQrgEJuqlOIejGgOq/onfgYZyMkDVgAV0DMtCgy+VHZbpNbRYjY7plDVgABqSyhYl4Am5MSA5MbVcboDBIm1tRLR0YbNClUBKPCWkJ1TRxfwvVM3TNsgPkuPhv6HkQOekGNS6eW8OFS3XQUNGCiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiJw6Tk7+BhBm97JkxQyZAAAAAElFTkSuQmCC"

/***/ }),

/***/ 544:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(333);


/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiClientService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ApiClientService = (function () {
    function ApiClientService(http, router) {
        this.http = http;
        this.router = router;
        this.url = "//api.spotware-talent.local";
        this._apiKey = null;
    }
    ApiClientService.prototype.apiKey = function () {
        return this._apiKey;
    };
    ApiClientService.prototype.loggedIn = function () {
        return this._apiKey ? true : false;
    };
    ApiClientService.prototype.logout = function () {
        this._apiKey = null;
        this.router.navigate(['/overview']);
    };
    ApiClientService.prototype.login = function (username, password) {
        var url = this.url + "/auth/login";
        var that = this;
        var response = this.post(url, { username: username, password: password });
        response.subscribe(function (response) {
            that._apiKey = response.data;
            that.router.navigate(['/books']);
        });
        return response;
    };
    // return all books
    ApiClientService.prototype.getBooks = function (offset, limit) {
        var url = this.url + "/book/?offset=" + offset + "&limit=" + limit;
        return this.get(url);
    };
    // create or update book
    ApiClientService.prototype.saveBook = function (book) {
        var url = this.url + "/book/" + (book['ISBN'] ? book['ISBN'] : '0');
        return this.post(url, book);
    };
    // delete book
    ApiClientService.prototype.deleteBook = function (book) {
        var url = this.url + "/book/" + book['ISBN'];
        return this.delete(url);
    };
    // return all users
    ApiClientService.prototype.getUsers = function (offset, limit) {
        var url = this.url + "/user/?offset=" + offset + "&limit=" + limit;
        return this.get(url);
    };
    // create or update user
    ApiClientService.prototype.saveUser = function (user) {
        var url = this.url + "/user/" + (user['User-ID'] ? user['User-ID'] : '0');
        return this.post(url, user);
    };
    // delete user
    ApiClientService.prototype.deleteUser = function (user) {
        var url = this.url + "/user/" + user['User-ID'];
        return this.delete(url);
    };
    ApiClientService.prototype.get = function (url) {
        return this.http.get(url)
            .map(this.handleResponse)
            .catch(this.handleError);
    };
    ApiClientService.prototype.post = function (url, data) {
        url += '?apikey=' + this.apiKey();
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers });
        return this.http.post(url, this.arrayToQueryString(data), options)
            .map(this.handleResponse)
            .catch(this.handleError);
    };
    ApiClientService.prototype.delete = function (url) {
        url += '?apikey=' + this.apiKey();
        return this.http.delete(url)
            .map(this.handleResponse)
            .catch(this.handleError);
    };
    ApiClientService.prototype.handleResponse = function (res) {
        var body = res.json();
        if (body.error) {
            console.log(body);
        }
        return body || {};
    };
    ApiClientService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Response */]) {
            var body = error.json() || '';
            var err = body.message || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + "\n" + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        alert(errMsg);
        return Promise.reject(errMsg);
    };
    ApiClientService.prototype.arrayToQueryString = function (array_in) {
        var out = new Array();
        for (var key in array_in) {
            out.push(key + '=' + encodeURIComponent(array_in[key]));
        }
        return out.join('&');
    };
    ApiClientService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Http */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === 'function' && _b) || Object])
    ], ApiClientService);
    return ApiClientService;
    var _a, _b;
}());
//# sourceMappingURL=api-client.service.js.map

/***/ })

},[544]);
//# sourceMappingURL=main.bundle.js.map