import { Component, Directive, OnInit, Input } from '@angular/core';
import { Book } from '../books/book';
import { ApiClientService } from '../api-client.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  title: string = "Books Gallery";

  books: Book[];
  offset: number = 0;
  limit: number = 25;
  booksCount: number = 9999; // overall count

  constructor(private client: ApiClientService) { 
    document.title = this.title;
  }

  getBooks() {
      let that = this;
      this.client.getBooks(this.offset, this.limit)
      .subscribe(function(response) {
        that.books = response.data; 
        that.booksCount = response.count; 
      });
  }

  ngOnInit() {
    this.getBooks();
  }

  pageChanged(page) {
    this.offset = page * this.limit - this.limit;
    this.getBooks();
    //console.log("page changed" + pageId);
  }

}


