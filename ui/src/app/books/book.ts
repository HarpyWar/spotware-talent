export class Book
{
	'Book-Author': string;
	'Book-Title': string;
	'ISBN': string;
	'Publisher': string;
	'Year-Of-Publication': number;
	'Image-URL-L': string;
	'Image-URL-M': string;
	'Image-URL-S': string;
	editMode: boolean = false;
}