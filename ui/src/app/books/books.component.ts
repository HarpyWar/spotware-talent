import { Component, OnInit } from '@angular/core';
import { Book } from '../books/book';
import { ApiClientService } from '../api-client.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  title: string = "Manage Books";

  books: Book[];
  offset: number = 0;
  limit: number = 25;
  booksCount: number = 9999; // overall count


  constructor(private client: ApiClientService) { 
    document.title = this.title;
  }

  getBooks() {
      let that = this;
      this.client.getBooks(this.offset, this.limit)
        .subscribe(function(response) {
          that.books = response.data; 
          that.books.push(new Book()); // add new book to the end to show a creation form
          that.booksCount = response.count; 
        });
  }

  createBook(book: Book) {
      book.editMode = null;
      let that = this;
      this.client.saveBook(book)
        .subscribe(function(response) {
          if (response.data) {
            let _book = Object.assign({}, book); // shallow copy
            _book['ISBN'] = response.data; // update id
            that.books.push(_book);
          }
          book.editMode = false;
        });
  }

  editBook(book: Book) {
      let that = this;
      this.client.saveBook(book)
        .subscribe(function(response) {
        });
  }

  deleteBook(book: Book) {
      let that = this;
      this.client.deleteBook(book)
        .subscribe(function(response) {
          that.books = that.books.filter(item => item !== book);
        });
  }


  ngOnInit() {
    this.getBooks();
  }

  pageChanged(page) {
    this.offset = page * this.limit - this.limit;
    this.getBooks();
    //console.log("page changed" + pageId);
  }

}
