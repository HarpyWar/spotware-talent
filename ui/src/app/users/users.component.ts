import { Component, OnInit } from '@angular/core';
import { User } from '../users/user';
import { ApiClientService } from '../api-client.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  title: string = "Manage Users";


  users: User[];
  offset: number = 0;
  limit: number = 25;
  usersCount: number = 9999; // overall count


  constructor(private client: ApiClientService) { 
    document.title = this.title;
  }

  getUsers() {
      let that = this;
      this.client.getUsers(this.offset, this.limit)
        .subscribe(function(response) {
          that.users = response.data; 
          that.users.push(new User()); // add new user to the end to show a creation form
          that.usersCount = response.count; 
        });
  }

  createUser(user: User) {
      user.editMode = null;
      let that = this;
      this.client.saveUser(user)
        .subscribe(function(response) {
          if (response.data) {
            let _user = Object.assign({}, user); // shallow copy
            _user['User-ID'] = response.data; // update id
            that.users.push(_user);
          }
          user.editMode = false;
        });
  }

  editUser(user: User) {
      let that = this;
      this.client.saveUser(user)
        .subscribe(function(response) {
        });
  }

  deleteUser(user: User) {
      let that = this;
      this.client.deleteUser(user)
        .subscribe(function(response) {
          that.users = that.users.filter(item => item !== user);
        });
  }


  ngOnInit() {
    this.getUsers();
  }

  pageChanged(page) {
    this.offset = page * this.limit - this.limit;
    this.getUsers();
    //console.log("page changed" + pageId);
  }
}
