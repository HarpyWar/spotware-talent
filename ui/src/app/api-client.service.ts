import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Book } from './books/book';
import { User } from './users/user';

@Injectable()
export class ApiClientService {
	private url = "//api.spotware-talent.local";
	private _apiKey: string = null;

	constructor(private http: Http, private router: Router) { }

	apiKey(): string {
		return this._apiKey;
	}

	loggedIn(): boolean {
		return this._apiKey ? true : false;
	}

	logout() {
		this._apiKey = null;
		this.router.navigate(['/overview']);
	}

	login(username: string, password: string): Observable<any> {
		let url = this.url + "/auth/login";
		let that = this;
		let response = this.post(url, { username: username, password: password });
		response.subscribe(function(response){
			that._apiKey = response.data;
			that.router.navigate(['/books']);
		});
		return response;
	}

	// return all books
	getBooks(offset: number, limit: number): Observable<any> {
		let url = this.url + "/book/?offset=" + offset + "&limit=" + limit;
		return this.get(url);
	}

	// create or update book
	saveBook(book: Book): Observable<any> {
		let url = this.url + "/book/" + (book['ISBN'] ? book['ISBN']: '0');
		return this.post(url, book);
	}

	// delete book
	deleteBook(book: Book): Observable<any> {
		let url = this.url + "/book/" + book['ISBN'];
		return this.delete(url);
	}


	// return all users
	getUsers(offset: number, limit: number): Observable<any> {
		let url = this.url + "/user/?offset=" + offset + "&limit=" + limit;
		return this.get(url);
	}

	// create or update user
	saveUser(user: User): Observable<any> {
		let url = this.url + "/user/" + (user['User-ID'] ? user['User-ID']: '0');
		return this.post(url, user);
	}

	// delete user
	deleteUser(user: User): Observable<any> {
		let url = this.url + "/user/" + user['User-ID'];
		return this.delete(url);
	}


	get(url: string): Observable<any> {
		return this.http.get(url)
			.map(this.handleResponse)
			.catch(this.handleError);
	}
	post(url: string, data: any): Observable<any> {
		url += '?apikey=' + this.apiKey();
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); 
        let options = new RequestOptions({ headers: headers });

		return this.http.post(url, this.arrayToQueryString(data), options)
			.map(this.handleResponse)
			.catch(this.handleError);
	}
	delete(url: string): Observable<any> {
		url += '?apikey=' + this.apiKey();
		return this.http.delete(url)
			.map(this.handleResponse)
			.catch(this.handleError);
	}

	handleResponse(res: Response) {
		let body = res.json();
		if (body.error) {
			console.log(body);
		}
		return body || { };
	}

	handleError (error: Response | any) {
		// In a real world app, we might use a remote logging infrastructure
		let errMsg: string;
		if (error instanceof Response) {
			const body = error.json() || '';
			const err = body.message || JSON.stringify(body);
			errMsg = `${error.status} - ${error.statusText || ''}\n${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		console.error(errMsg);
		alert(errMsg);
		return Promise.reject(errMsg);
	}


	private arrayToQueryString(array_in){
		var out = new Array();

		for(var key in array_in){
			out.push(key + '=' + encodeURIComponent(array_in[key]));
		}

		return out.join('&');
	}
}