import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

// how many page numbers to display
const PAGE_NAVIGATOR_PAGE_NUMBERS: number = 11;

@Component({
  selector: 'page-navigator',
  template: `
	<div class="ui right floated pagination menu">
        <a class="icon item" *ngIf="pageNumbers[0] != 1" (click)="selectPage(1)">
          <b>First</b>
        </a>
        <a class="icon item" *ngIf="page != 1" (click)="prevPage()">
          <i class="left chevron icon"></i>
        </a>

        <a class="item" *ngFor="let n of pageNumbers" [ngClass]="{'active': n == page}"  (click)="selectPage(n)">{{ n }}</a>
        
		<a class="icon item" *ngIf="page != maxPage()" (click)="nextPage()">
          <i class="right chevron icon"></i>
        </a>
        <a class="icon item" *ngIf="pageNumbers[pageNumbers.length-1] != maxPage()" (click)="selectPage(maxPage())">
          <b>Last</b>
        </a>
	</div>
	<div class="clear"></div>
	`
})
export class PageNavigatorComponent implements OnInit {
	@Input() offset: number;
	@Input() limit: number;
	@Input() count: number;
	@Output() onPageChange: EventEmitter<any> = new EventEmitter();
	
	page: number;
	pageNumbers: number[] = [];

	ngOnInit() {
		// determine current page
		this.page = Math.ceil(this.offset / this.limit) + 1;
		this.updatePageNumbers();
	}

	maxPage(): number {
		return Math.ceil(this.count / this.limit);
	}

	selectPage(pageId) {
		this.page = pageId;
		if (this.page <= 0)
		{
			this.page = 1;
		}
		if (this.page > this.maxPage())
		{
			this.page = this.maxPage();
		}
		this.updatePageNumbers();
		this.onPageChange.emit(this.page);
		return false;
	}

	nextPage() {
		this.selectPage(this.page + 1);
	}
	prevPage() {
		this.selectPage(this.page - 1);
	}


	// return current page numbers
	updatePageNumbers() {
		this.pageNumbers = [];
		let start = this.page - Math.floor(PAGE_NAVIGATOR_PAGE_NUMBERS / 2);
		let end = this.page + Math.floor(PAGE_NAVIGATOR_PAGE_NUMBERS / 2);
		if (end > this.maxPage()) {
			start -= end - this.maxPage();
			end = this.maxPage();
		}
		if (start <= 0) {
			start = 1;
		}
		// FIXME: wrong result with small count
		if (end - start < PAGE_NAVIGATOR_PAGE_NUMBERS-1)
		{
			end = PAGE_NAVIGATOR_PAGE_NUMBERS;
		}
		if (end - start < PAGE_NAVIGATOR_PAGE_NUMBERS-1)
		{
			start = 1;
		}
		
		for (let i = start; i <= end; i++) {
			this.pageNumbers.push(i);
		}
		//console.log(this.page, this.maxPage(), start, end);
	
	}
}
