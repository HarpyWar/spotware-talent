import { Component } from '@angular/core';
import { ApiClientService } from './api-client.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ApiClientService]
})
export class AppComponent {
  constructor(private client: ApiClientService) { }
  
}
