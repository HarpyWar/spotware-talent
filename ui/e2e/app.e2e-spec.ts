import { BooksUiPage } from './app.po';

describe('books-ui App', () => {
  let page: BooksUiPage;

  beforeEach(() => {
    page = new BooksUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
