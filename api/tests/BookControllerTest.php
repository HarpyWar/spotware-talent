<?php
ob_start();
use Silex\WebTestCase;

/**
 * Created by PhpStorm.
 * User: harpy
 * Date: 23.03.2017
 * Time: 13:49
 */
class BookControllerTest extends WebTestCase
{
    public function setUp() {
        parent::setUp();
        // Add your code here...
    }

    public function createApplication()
    {
        require __DIR__.'/../index.php';
        //$app['debug'] = true;
        //unset($app['exception_handler']);
        return $app;
    }

    public function testGetBooks()
    {
        $client = $this->createClient();
        $client->request('GET', '/book/');

        $this->assertTrue($client->getResponse()->isOk());
        // contents
        $json = json_decode($client->getResponse()->getContent());
        $this->assertTrue(isset($json->count));
        $this->assertTrue(isset($json->offset));
        $this->assertTrue(isset($json->limit));
        $this->assertTrue(isset($json->data));
        $this->assertTrue(isset($json->success));
    }

    public function testCreateUpdateDeleteBook()
    {
        $id = false;
        $author = "Author1";
        $year = 2017;
        $publisher = "Publisher1";
        $title = "Title1";
        $image_s = "ImageS";
        $image_m = "ImageM";
        $image_l = "ImageL";

        // create
        $model = $this->app['model']['book'];
        $id = $model->SaveBook($id, $author, $year, $publisher, $title, $image_s, $image_m, $image_l);
        $this->assertTrue(strlen($id) == 10); // new id

        // check created
        $book = $model->GetBook($id);
        $this->assertTrue($book['Book-Author'] == $author);
        $this->assertTrue($book['Year-Of-Publication'] == $year);
        $this->assertTrue($book['Publisher'] == $publisher);
        $this->assertTrue($book['Image-URL-S'] == $image_s);
        $this->assertTrue($book['Image-URL-M'] == $image_m);
        $this->assertTrue($book['Image-URL-L'] == $image_l);

        // update
        $result = $model->SaveBook($id, $author.'2', $year.'2', $publisher.'2', $title.'2', $image_s.'2', $image_m.'2', $image_l.'2');
        $this->assertTrue($result == 1);
        
        // check updated
        $book = $model->GetBook($id);
        $this->assertTrue($book['Book-Author'] == $author.'2');
        $this->assertTrue($book['Year-Of-Publication'] == $year.'2');
        $this->assertTrue($book['Publisher'] == $publisher.'2');
        $this->assertTrue($book['Image-URL-S'] == $image_s.'2');
        $this->assertTrue($book['Image-URL-M'] == $image_m.'2');
        $this->assertTrue($book['Image-URL-L'] == $image_l.'2');

        // delete
        $result = $model->DeleteBook($id);
        $this->assertTrue($result == 1);
    }

    private function apiKeyQuery()
    {
        return '?apikey=' . $this->app['config']['account']['apikey'];
    }

}
