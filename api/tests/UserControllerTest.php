<?php
ob_start();
use Silex\WebTestCase;

/**
 * Created by PhpStorm.
 * User: harpy
 * Date: 23.03.2017
 * Time: 13:49
 */
class useruserControllerTest extends WebTestCase
{
    public function setUp() {
        parent::setUp();
        // Add your code here...
    }

    public function createApplication()
    {
        require __DIR__.'/../index.php';
        return $app;
    }

    public function testGetUsers()
    {
        $client = $this->createClient();
        $client->request('GET', '/user/');

        $this->assertTrue($client->getResponse()->isOk());
        // contents
        $json = json_decode($client->getResponse()->getContent());
        $this->assertTrue(isset($json->count));
        $this->assertTrue(isset($json->offset));
        $this->assertTrue(isset($json->limit));
        $this->assertTrue(isset($json->data));
        $this->assertTrue(isset($json->success));
    }

    public function testCreateUpdateDeleteUser()
    {
        $id = false;
        $location = "usa";
        $age = 99;

        // create
        $model = $this->app['model']['user'];
        $id = $model->SaveUser($id, $location, $age);
        $this->assertTrue($id > 0); // new id

        // check created
        $user = $model->GetUser($id);
        $this->assertTrue($user['Location'] == $location);
        $this->assertTrue($user['Age'] == $age);

        // update
        $result = $model->SaveUser($id, $location.'2', $age.'2');
        $this->assertTrue($result == 1);

        // check updated
        $user = $model->GetUser($id);
        $this->assertTrue($user['Location'] == $location.'2');
        $this->assertTrue($user['Age'] == $age.'2');

        // delete
        $result = $model->DeleteUser($id);
        $this->assertTrue($result == 1);
    }

    private function apiKeyQuery()
    {
        return '?apikey=' . $this->app['config']['account']['apikey'];
    }

}
