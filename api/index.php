<?php

require_once ("vendor/autoload.php");

use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();
$app->register(new DerAlex\Silex\YamlConfigServiceProvider(__DIR__ . '/settings.yml'));

$app['debug'] = $app['config']['debug'];
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => $app['config']['database']
));


require_once('private/models/DbModel.php');
require_once('private/models/BookModel.php');
require_once('private/models/UserModel.php');
require_once('private/controllers/BookController.php');
require_once('private/controllers/UserController.php');
require_once('private/controllers/AuthController.php');

// register models
$app['model'] = function() use ($app){
    return array(
        "book" => new BookModel($app),
        "user" => new UserModel($app),
    );
};

// handle errors
$app->error(function (\Exception $e, $code) use ($app) {
    $message = $e->getMessage();
    return $app->json(array(
        "error" => true,
        "message" => $message
    ), $code);
});

// format output response
$app['success'] = function () use ($app)
{
    return function($data, $code = Response::HTTP_OK) use ($app) {
        $result = array(
            "success" => true,
            "data" => $data
        );
        if (is_array($data))
        {
            $result = array_merge($result, $data);
        }
        return $app->json($result, $code);
    };
};

// authorization handler
$app['auth'] = function () use ($app)
{
    if (!$app['request']->get('apikey') || $app['request']->get('apikey') != $app['config']['account']['apikey'])
    {
        throw new Exception("Not Authorized");
    }
};

// cors headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
header("Access-Control-Allow-Methods: GET,POST,DELETE");

// stop with code 200 to support cors DELETE method ()
if (array_key_exists('REQUEST_METHOD', $_SERVER) && $_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    exit;
};


$app->mount('/book', new BookController());
$app->mount('/user', new UserController());
$app->mount('/auth', new AuthController());

$app->run();


