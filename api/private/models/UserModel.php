<?php


class UserModel extends DbModel
{
    public function GetUsers($offset, $limit)
    {
        $sql = sprintf("SELECT * FROM `BX-Users` LIMIT %d,%d", $offset, $limit);
        $result = $this->db->fetchAll($sql, array((int) $limit));
        return $result;
    }

    public function GetUser($id)
    {
        $sql = "SELECT * FROM `BX-Users` WHERE `User-ID` = ?";
        $result = $this->db->fetchAssoc($sql, array($id));
        return $result;
    }

    public function SaveUser($id, $location, $age)
    {
        // create
        if (!$id)
        {
            $id = $this->getId();
            $result = $this->db->insert("`BX-Users`", array('`User-ID`' => $id, '`Location`' => $location, '`Age`' =>  $age));
            $result = $id; // return new id
        }
        // update
        else
        {
            $result = $this->db->update("`BX-Users`", array('`Location`' => $location, '`Age`' =>  $age),
                array('`User-ID`' => $id) );
        }
        return $result;
    }

    public function DeleteUser($id)
    {
        $result = $this->db->delete("`BX-Users`", array('`User-ID`' => $id) );
        return $result;
    }

    public function GetUsersCount()
    {
        $result = $this->db->fetchColumn("SELECT COUNT(*) FROM `BX-Users`");
        return $result;
    }

    /*
     * Return id for a new user
     */
    private function getId()
    {
        $id = $this->db->fetchColumn("SELECT MAX(`User-ID`) FROM `BX-Users`");
        return ++$id;
    }

}