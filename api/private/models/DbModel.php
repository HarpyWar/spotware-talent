<?php

use Silex\Application;

class DbModel
{
    protected $db;

    public function __construct(Application $app)
    {
        $this->db = $app['db'];
    }

}