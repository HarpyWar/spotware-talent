<?php


class BookModel extends DbModel
{
    public function GetBooks($offset, $limit)
    {
	

        $sql = sprintf("SELECT AVG(`Book-Rating`) AS `Book-Rating`, `BX-Books`.* 
                        FROM `BX-Books` 
                        LEFT JOIN `BX-Book-Ratings` USING (`ISBN`)
                        WHERE `BX-Book-Ratings`.`Book-Rating` <> 0 
                        GROUP BY `BX-Book-Ratings`.`ISBN` 
                        LIMIT %d,%d", $offset, $limit);
        $result = $this->db->fetchAll($sql, array((int) $limit));
        return $result;
    }

    public function GetBook($id)
    {
        $sql = "SELECT * FROM `BX-Books` WHERE ISBN = ?";
        $result = $this->db->fetchAssoc($sql, array($id));
        return $result;
    }


    public function SaveBook($id, $author, $year, $publisher, $title, $image_s, $image_m, $image_l)
    {
        // create
        if (!$id)
        {
            $id = $this->getId();
            $result = $this->db->insert("`BX-Books`", array('`ISBN`' => $id, '`Book-Title`' => $title, '`Book-Author`' =>  $author, '`Year-Of-Publication`' => (int)$year, '`Publisher`' => $publisher, '`Image-URL-S`' => $image_s, '`Image-URL-M`' => $image_m, '`Image-URL-L`' => $image_l));
            // check for possible duplicate id
            if ($result == 1)
            {
                $result = $id; // return new id
            }
        }
        // update
        else
        {
            $result = $this->db->update("`BX-Books`", array('`Book-Title`' => $title, '`Book-Author`' =>  $author, '`Year-Of-Publication`' => (int)$year, '`Publisher`' => $publisher, '`Image-URL-S`' => $image_s, '`Image-URL-M`' => $image_m, '`Image-URL-L`' => $image_l),
                array('`ISBN`' => $id) );
        }
        return $result;
    }

    public function DeleteBook($id)
    {
        $result = $this->db->delete("`BX-Books`", array('`ISBN`' => $id) );
        return $result;
    }

    public function GetBooksCount()
    {
        $result = $this->db->fetchColumn("SELECT COUNT(*) FROM `BX-Books`");
        return $result;
    }

    /*
     * Return random id for a new book
     */
    private function getId()
    {
        $value = rand(0000000001, 9999999999);
        return str_pad($value, 10, "0", STR_PAD_LEFT);
    }
}