<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\ControllerProviderInterface;
use Silex\Application;

class BookController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $factory = $app['controllers_factory'];
        $factory->get('/', 'BookController::get');

        $factory->post('/{id}', 'BookController::save')
            ->value('id', 0) // default id (new);
            ->before(function() use ($app) { $app['auth']; });
        $factory->delete('/{id}', 'BookController::delete')
            ->before(function() use ($app) { $app['auth']; });

        return $factory;
    }

    /*
     * Return list of books
     * GET /book?offset=0&limit=10
     */
    public function get(Application $app, Request $request)
    {
        $offset = $request->get('offset') ?: 0;
        $limit = $request->get('limit') ?: 10;
        $limit = $limit > 1000 ? 1000 : $limit;

        $result = array(
            'data' => $app['model']['book']->GetBooks($offset, $limit),
            'offset' => $offset,
            'limit' => $limit,
            'count' => $app['model']['book']->GetBooksCount()
        );

        return $app['success']($result);
    }

    /*
     * Create (if id not given) or update book by id
     * POST /book/12345ABC
     * (return new id or affected rows count)
     */
    public function save(Application $app, Request $request, $id)
    {
        // all fields are required
        $author = $request->get('Book-Author');
        $year = $request->get('Year-Of-Publication');
        $publisher = $request->get('Publisher');
        $title = $request->get('Book-Title');
        $image_s = $request->get('Image-URL-S');
        $image_m = $request->get('Image-URL-M');
        $image_l = $request->get('Image-URL-L');

        $result = $app['model']['book']->SaveBook($id, $author, $year, $publisher, $title, $image_s, $image_m, $image_l);
        return $app['success']($result);
    }

    /*
     * Delete user by id
     * DELETE /user/12345
     * (return affected rows count)
     */
    public function delete(Application $app, Request $request, $id)
    {
        $result = $app['model']['book']->DeleteBook($id);
        return $app['success']($result); // return affected rows count
    }
}
