<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\ControllerProviderInterface;
use Silex\Application;

class AuthController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $factory = $app['controllers_factory'];
        $factory->post('/login', 'AuthController::login');

        return $factory;
    }

    /*
     * Validate given username and password
     * POST /auth/login
     * (return apikey if success)
     */
    public function login(Application $app, Request $request)
    {
        // all fields are required
        $username = $request->get('username');
        $password = $request->get('password');

        if ($username != $app['config']['account']['username'] || $password != $app['config']['account']['password'])
        {
            throw new Exception("Incorrect username or password");
        }
        return $app['success']($app['config']['account']['apikey']);
    }

}