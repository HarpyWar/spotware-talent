<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\ControllerProviderInterface;
use Silex\Application;

class UserController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $factory = $app['controllers_factory'];
        $factory->get('/', 'UserController::get');

        $factory->post('/{id}', 'UserController::save')
            ->value('id', 0) // default id (new);
            ->before(function() use ($app) { $app['auth']; });
        $factory->delete('/{id}', 'UserController::delete')
            ->before(function() use ($app) { $app['auth']; });

        return $factory;
    }

    /*
     * Return list of users
     * GET /user?offset=0&limit=10
     */
    public function get(Application $app, Request $request)
    {
        $offset = $request->get('offset') ?: 0;
        $limit = $request->get('limit') ?: 10;
        $limit = $limit > 1000 ? 1000 : $limit;

        $result = array(
            'data' => $app['model']['user']->GetUsers($offset, $limit),
            'offset' => $offset,
            'limit' => $limit,
            'count' => $app['model']['user']->GetUsersCount()
        );
        return $app['success']($result);
    }

    /*
     * Create (if id not given) or update user by id
     * POST /user/12345
     * (return new id or affected rows count)
     */
    public function save(Application $app, Request $request, $id)
    {
        // all fields are required
        $location = $request->get('Location');
        $age = $request->get('Age');

        $result = $app['model']['user']->SaveUser($id, $location, $age);
        return $app['success']($result);
    }

    /*
     * Delete user by id
     * DELETE /user/12345
     * (return affected rows count)
     */
    public function delete(Application $app, Request $request, $id)
    {
        $result = $app['model']['user']->DeleteUser($id);
        return $app['success']($result); // return affected rows count
    }
    
}